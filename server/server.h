#ifndef SERVER_H
	#define SERVER_H

	// macro to simplify error handling
	#define GENERIC_ERROR_HELPER(cond, errCode, msg) do {           \
        if (cond) {                                                 \
            fprintf(stderr, "%s: %s\n", msg, strerror(errCode));    \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

	#define ERROR_HELPER(ret, msg)          GENERIC_ERROR_HELPER((ret < 0), errno, msg)
	#define PTHREAD_ERROR_HELPER(ret, msg)  GENERIC_ERROR_HELPER((ret != 0), ret, msg)

	/* Configuration parameters */
	#define DEBUG               1   // display debug messages
	#define SERVER_COMMAND      "quit"
	#define SERVER_PORT         2015
    #define MAX_CONN_QUEUE      5
    //boolean
    #define FALSE               0
    #define TRUE                1
    //server comands
    #define PHOTO               "searchphoto"
    #define PHOTO_LEN           strlen(PHOTO)
    //photo commands
    #define FOUND               "found"
    #define FOUND_LEN           strlen(FOUND)
    #define NOT_FOUND           "notfound"
    #define NOT_FOUND_LEN       strlen(NOT_FOUND)
    //messages
    #define WELCOME_MESS        "Hi! I'm the Nardo Server :)\nWhat do you want to do? I will stop if you send me quit\n"
    #define PHOTO_MESS          "Please choose a photo from the list belove:\n"
    #define PHOTONOTFOUND_MESS  "File not found,please chose a photo from the list below:\n"
    #define WELLDONE_MESS       "Well Done\n"
    #define NOGOOD_MESS         "Invalid input!\n"
    
    
    /** VARIABLES FOR SURVEILLANCE*/
    #define SURVEILLANCE_MESS "\nWelcome to the videosurveillance nardo's system.\nStart videosurveillance system?[ Yes | No ]"
    #define STARTVIDEO_MESS "\nNARDOVIDEOSURVEILLANCE START!\n"
    #define ALLARM_MESS "\nAllarm! Allarm! Allarm!\n Don't worry we saved the image on photo_detected \n\n"
    //server commands
    #define SURVEILLANCE "videosurveillance"
    #define SURVEILLANCE_LEN strlen(SURVEILLANCE)

    //settings commands
    #define YES "yes"
    #define YES_LEN strlen(YES)
    #define NO "no"
    #define NO_LEN strlen(NO)
    
    
    #define THRESHOLD_MESS "Set treshold\n 5: [++PRECISION--ERROR] \n 15: [+PRECISION-ERROR] \n 30: [--PRECISION--ERROR] : "
    

#endif
