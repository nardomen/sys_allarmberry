#include <cv.h>
#include <highgui.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

//non mi piace
#include "there_is_movement.h"
#include <time.h>
void get_the_power(){
	system("chmod 666 files/*.jpg");
}

int there_is_movement(int threshold,int count){
	 //variable for directory and file 
	 char file_name[512];
	 char second_name[512];
	 size_t file_name_len = strlen(&file_name);
	 size_t second_name_len = strlen(&second_name);
	 
	 DIR *dir;
	 struct dirent *ent;
	 
	 //variable for images
	 IplImage* img_gray;
	 IplImage* img_bew;
	 IplImage* img2_gray;
	 IplImage* img2_bew;
	 IplImage* motion_detected;
	 
	 int debug=1;
	 
	 get_the_power();
	 //è un semplice modo per listare i file all'interno di una cartella:
	 /*
	  * Praticamente si apre la cartella DIR tramite opendir, e ciò che restituisce viene passato
	  * a readdir il quale restituisce una struct dirent dove al suo interno possiamo trovare appunto tutto i file listati tramite la struct
	  * ah ovviamente la funzione readdir smette di funzionare quando si incontra NULL ovvero quando ha listato tutti i file
	   */
	   
	 //CONVERSION IN BeW foto principale
	 img_gray = cvLoadImage( IMAGE_1,CV_LOAD_IMAGE_GRAYSCALE);
	 img_bew = cvCreateImage(cvGetSize(img_gray),IPL_DEPTH_8U,1);
	 cvThreshold(img_gray,img_bew,128,255,CV_THRESH_BINARY | CV_THRESH_OTSU);
	 
	 sprintf(second_name,"image%d.jpg",count);
	 second_name_len = sizeof(&second_name);
	 if((dir = opendir("files/")) != NULL) {
		while((ent = readdir (dir)) != NULL ) {
			if (memcmp(ent->d_name,"..",2) && memcmp(ent->d_name,".",1)) {
				
				//bisogna che catturi solo image3.jpg e non image2.jpg \n e image3.jpg "
				memset(file_name, 0 , file_name_len);
				sprintf(file_name,"%s\n",ent->d_name);
				file_name_len = strlen(&file_name);
				printf("vediamo che file abbiamo carpito con file_name : %s \n",file_name);	
				//strncmp returns 0 if the name are the same
				file_name[--file_name_len] = '\0';
				
					printf("\nprima del confronto tra stringhe come sono le nostre variabili?\n");
					printf("\nfile_name : %s \nsecond_name : %s \n",file_name,second_name);
					
				if(!strncmp(file_name,second_name,file_name_len)){
					
					//clean second_name for adding the correct path to read image
					memset(second_name,0,second_name_len);
					sprintf(second_name,"files/%s",file_name);
					//we need to leave \n on the variable
					
					
					printf("\nprima del cv_load come sono le nostre variabili?\n");
					printf("\nfile_name : %s \nsecond_name : %s \n",file_name,second_name);
					
					sleep(3);
					//do in this way to keep the original color photo to save it to see better
					motion_detected = cvLoadImage(second_name, CV_LOAD_IMAGE_COLOR);
					img2_gray = cvCreateImage(cvGetSize(motion_detected),IPL_DEPTH_8U,1);
					cvCvtColor(motion_detected,img2_gray,CV_RGB2GRAY);
					img2_bew = cvCreateImage(cvGetSize(img2_gray),IPL_DEPTH_8U,1);
					cvThreshold(img2_gray,img2_bew,0,255,CV_THRESH_BINARY | CV_THRESH_OTSU);
					debug=0;
					goto SALTA_CICLO;
				} //skip "." and ".." directories
			}
		}
	 }
	 
SALTA_CICLO:
	if(debug)
		printf("no");

	
	//ok go inside the photo, wanna meet the pixel? 
	int tmp;
	int tmp2;
	int countE=0;
	
	int size=img_bew->height*img_bew->width;
	
	for(int r = 0; r < img_bew->height;r++)
	{
		for(int c = 0; c < img_bew->width; c++)
		{
			tmp=cvGet2D(img_bew,r,c).val[0];
			tmp2=cvGet2D(img2_bew,r,c).val[0];
			if(tmp != tmp2)
				countE++;
		}
	}
	
	if(countE < (threshold*(size))/100){
		printf("\n NO MOVEMENT - NEW FRAME PROCESS -\n");
		return 1;		//for while 1 is considered as true 
	}
	else{
		/*
		 * MOTION DETECTED
		 * */
		 //use this way to save with a name that always change!
		 printf("\n MOVEMENT DETECT \n");
		 time_t data;
		 struct tm *time_line = NULL;
		 time(&data);
		 time_line = localtime(&data);
		 memset(second_name,0,second_name_len);
		 //cvSaveImage save the photo into the correct dir
		 sprintf(second_name,"files/files_%d_%d_%d|%d:%d:%d.jpg",time_line->tm_mday,time_line->tm_mon+1,time_line->tm_year+1900, time_line->tm_hour, time_line->tm_min, time_line->tm_sec);
		 cvSaveImage(second_name,motion_detected,0);
		 
		
		 /* 
		 system("rm -rf ./files/image1.jpg");
		 cvSaveImage(IMAGE_1,motion_detected);
		 */
		 
		return 0;		//for while 0 is considered as false
	}
	 
	 
	 //free space
		 cvReleaseImage(&motion_detected);
		 cvReleaseImage(&img_bew);
		 cvReleaseImage(&img2_gray);
		 cvReleaseImage(&img2_bew);
		 cvReleaseImage(&img_gray);
		

}

