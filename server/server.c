#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>     //per i segnali
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>  // htons()
#include <netinet/in.h> // struct sockaddr_in
#include <sys/socket.h>
#include <semaphore.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>

#include "server.h"
#include "takePhoto.h"
#include "poll.h"
#include "there_is_movement.h"

//Semaphore
sem_t Rasputin;
int count = 1;

//struct thread arg
typedef struct handler_args_s{
    int socket_desc;
    struct sockaddr_in *client_addr;
}    handler_args_t;


//thread function
void *connection_handler(void *arg){
	int ret;
	int i;
	int index;
    int recv_bytes;
    
    //thread arg
    handler_args_t *args = (handler_args_t *)arg;        
    int socket_desc = args->socket_desc;
    struct sockaddr_in *client_addr = args->client_addr;
    //parse client IP address and port
    char client_ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(client_addr->sin_addr), client_ip, INET_ADDRSTRLEN);
    uint16_t client_port = ntohs(client_addr->sin_port); 
    //local variables
    char buf[1024];
    size_t buf_len = sizeof(buf);
    char aux_buf[8];
    size_t aux_buf_len = sizeof(aux_buf);
    size_t msg_len;
    char path[2048];
    size_t path_len =sizeof(path);
    char *quit_command = SERVER_COMMAND;
    size_t quit_command_len = strlen(quit_command);
    //control
    int control;
    
    //picture
    FILE *picture;
    char* send_buffer;
    int size;
    int read_bytes;
    int send_bytes;
    int aux;
    
    //directory
    DIR *dir;
    struct dirent *ent;
    
    /**   spazio per le variabili create per il sistema di videosorveglianza */
    int threshold;
    
    //initialize welcome message
    memset(buf , 0 , buf_len);
    /*if(DEBUG) 
    	fprintf(stderr, "buf: %s\n", buf);*/
    sprintf(buf , WELCOME_MESS);
    msg_len = strlen(buf);
    /*if(DEBUG) 
    	fprintf(stderr, "buf: %s\n", buf);*/
    //send welcome message
    while ((ret = send(socket_desc, buf, msg_len, 0)) < 0){
        if (errno == EINTR) continue;
        ret = close(socket_desc);
        //free buffer
    	free(args->client_addr); 
    	free(args);
    	//exit
    	pthread_exit(NULL);
    }
    //main loop
    while(1){
NS:   
		control = TRUE;
    	if(DEBUG)
    		fprintf(stderr, "Waiting options...\n");
    	//clear buf
    	memset(buf , 0 , buf_len);
    	/*if(DEBUG) 
    		fprintf(stderr, "buf: %s\n", buf);*/
    	//receive option from client
    	while ((recv_bytes = recv(socket_desc, buf, buf_len, 0)) < 0){
            if (errno == EINTR) continue;
            ret = close(socket_desc);
        	//free buffer
    		free(args->client_addr); 
    		free(args);
    		//exit
    		pthread_exit(NULL);
        }
        buf[recv_bytes] = '\0';
        if(DEBUG) 
    		fprintf(stderr, "buf: %s\n", buf);
        // check whether I have just been told to quit...
        if (recv_bytes == 0) break;
        if (recv_bytes == quit_command_len && !memcmp(buf, quit_command, quit_command_len)) break;
        /********************************
        *								*
        *			  PHOTO				*
        *								*
        *********************************/
        if (recv_bytes == PHOTO_LEN && !memcmp(buf,PHOTO,PHOTO_LEN)){
NF:        	//clear buf
    		memset(buf , 0 , buf_len);
    		/*if(DEBUG) 
    			fprintf(stderr, "buf: %s\n", buf);*/
    		//initilize photo mess
        	if(control){
        		sprintf(buf , PHOTO_MESS);
        		msg_len=strlen(buf);
        	}
        	else{
        		sprintf(buf , PHOTONOTFOUND_MESS);
        		msg_len = strlen(buf);
        	}
        	control = TRUE;
        	/*if(DEBUG) 
    			fprintf(stderr, "buf: %s\n", buf);*/
    		//directory scanner
            if ((dir = opendir("files/")) != NULL) {
                while ((ent = readdir (dir)) != NULL) {
                    if ( !memcmp(ent->d_name,"..",1) || !memcmp(ent->d_name,".",2)) continue;   //skip '.' and '..' directories
                    sprintf(&buf[msg_len],"%s\n", ent->d_name);
                    msg_len=strlen(buf);
                }
            }else{  //cannot show directory
                ERROR_HELPER(-1, "Cannot show directory");
            }
            /*
            if(DEBUG) 
    			fprintf(stderr, "buf: %s\n", buf);
    		*/
    		//send photo mess + list file directories
    		while ((ret = send(socket_desc, buf, msg_len, 0)) < 0){
        		if (errno == EINTR) continue;
        		ret = close(socket_desc);
        		//free buffer
    			free(args->client_addr); 
    			free(args);
    			//exit
    			pthread_exit(NULL);
    		}
    		//clear buf
    		memset(buf , 0 , buf_len);
			
			
    		//receive file name photo from client
    		while ((recv_bytes = recv(socket_desc, buf, buf_len, 0)) < 0){
				if (errno == EINTR) continue;
				ret = close(socket_desc);
        		//free buffer
    			free(args->client_addr); 
    			free(args);
    			//exit
    			pthread_exit(NULL);
        	}
        	if (DEBUG)
				printf("File name received from client %s",buf);
            //control to handle blank send
            if(recv_bytes < 2)
				if(memcmp(buf,'\0',1) == 0) goto NF;
        	/*if(DEBUG) 
    			fprintf(stderr, "buf: %s\n", buf);*/
    			
    		//clear file path
    		memset(path , 0 , path_len);
    		//create file path
            sprintf(path,"files/%s",buf);
            if(DEBUG)
    			fprintf(stderr, "path:%s\n", path);
    		//before we open it we need to leave '\0' from the word 
    		path_len = strlen(&path);
    		path[--path_len] = '\0';
    		
            //open file
    		if ((picture = fopen(path, "r")) != NULL){	//fhoto found
    			//clear buf
    			memset(buf , 0 , buf_len);
    			//inizialize found message
                sprintf(buf,FOUND);
                msg_len=FOUND_LEN;
                if(DEBUG) 
    				fprintf(stderr,"buf: %s\n", buf);
                //send found message
                while ((ret = send(socket_desc, buf, msg_len, 0)) < 0){
        			if (errno == EINTR) continue;
        			ret = close(socket_desc);
        			//free buffer
    				free(args->client_addr); 
    				free(args);
    				//exit
    				pthread_exit(NULL);
    			}
    			//wait on semaphore
            	ret = sem_wait(&Rasputin);
            	ERROR_HELPER(ret, "Wait on semaphore Rasputin");
            	if(DEBUG)
            		fprintf(stderr , "ON Semaphore\n");
            
    				
                //get picture size
                if(DEBUG)
                	fprintf(stderr , "Getting Picture Size\n");  
                fseek(picture, 0, SEEK_END);
                size = ftell(picture);
                fseek(picture,0, SEEK_SET);
                if(DEBUG)
                	fprintf(stderr, "size: %d\n", size);
                //send picture size
                if(DEBUG)
                	fprintf(stderr , "Sending Picture Size\n");
                while((ret = write(socket_desc, &size, sizeof(int))) < 0){
                	if(errno == EINTR) continue;
                	ret = close(socket_desc);
        			//free buffer
    				free(args->client_addr); 
    				free(args);
    				//exit
    				pthread_exit(NULL);
                }
                //send picture as byte array
                if(DEBUG)
                	fprintf(stderr , "Sending Picture as Byte Array\n");   
                //write picture on array        
                send_buffer = (char*)malloc(sizeof(char)*size);
                read_bytes = 0;
                aux = size;
                do{
                	ret = fread(send_buffer+read_bytes , 1 , aux , picture);
                	aux-=ret;
                }while(aux > 0);
                //send array
                send_bytes = 0;
                aux = size;
                do{
                	ret = write(socket_desc , send_buffer+send_bytes , aux);
                	if(ret == -1 && errno == EINTR)	continue;
                	else if(ret==-1){
                		ret = close(socket_desc);
        				//free buffer
    					free(args->client_addr); 
    					free(args);
    					//exit
    					pthread_exit(NULL);
                	}
                	aux-=ret;
                }while(aux > 0);
                //free picture array
                free(send_buffer);
                //post on semaphore
            	ret = sem_post(&Rasputin);
            	ERROR_HELPER(ret, "Wait on semaphore Rasputin");
            }else{							//photo not found
            	//clear buf
    			memset(buf , 0 , buf_len);
    			//if(DEBUG) 
    			//	fprintf(stderr, "buf: %s\n", buf);
    			//initialize not found mess
    			sprintf(buf , NOT_FOUND);
    			msg_len = NOT_FOUND_LEN;
    			if(DEBUG) 
    				fprintf(stderr, "buf: %s\n", buf);
    			//send not found mess
    			while ((ret = send(socket_desc, buf, msg_len, 0)) < 0){
        			if (errno == EINTR) continue;
        			ret = close(socket_desc);
        			//free buffer
    				free(args->client_addr); 
    				free(args);
    				//exit
    				pthread_exit(NULL);
    			}
    			control=FALSE;
                goto NF;
            }
        }		//photo done

		/****************************************
        *										*
        *			VIDEOSURVEILLANCE			*
        *										*
        ****************************************/
        else if ( recv_bytes = SURVEILLANCE_LEN && !memcmp(buf,SURVEILLANCE,SURVEILLANCE_LEN)){
			//clearbuf
			memset(buf, 0, buf_len);
			/*if(DEBUG) 
                fprintf(stderr, "buf: %s\n", buf);*/
			//inizialize buf
			sprintf(buf, SURVEILLANCE_MESS);
			msg_len = strlen(buf);
			/*if(DEBUG)
				fprintf(stderr, "buf :%s\n", buf);*/
            puts("\n User choose videosurveillance\n");
			//send start videosurveillance message 
			while ((ret = send(socket_desc, buf , msg_len , 0))<0){
				if (errno == EINTR) continue;
				ret = close(socket_desc);
				//free buffer
				free(args->client_addr);
				free(args);
				//exit
				pthread_exit(NULL);
			}
			//clear buff
			memset(buf , 0, buf_len);
			/*if(DEBUG) 
                fprintf(stderr, "buf: %s\n", buf);*/
			//receive YES or NOT from client
			while((recv_bytes = recv(socket_desc,buf,buf_len,0))<0){
				if(errno == EINTR) continue;
				ret = close(socket_desc);
				//free buffer
				free(args->client_addr);
				free(args);
				//exit
				pthread_exit(NULL);
			}
			buf[recv_bytes] = '\0';
            if(DEBUG) 
                fprintf(stderr, "buf: %s\n", buf);
			//YES resolution
			if(recv_bytes == YES_LEN && !memcmp(buf, YES, YES_LEN)){
									
				//clean buf
				memset(buf,0,buf_len);
				/*if(DEBUG) 
                    fprintf(stderr, "buf: %s\n", buf);*/
				sprintf(buf,THRESHOLD_MESS);
                //send request of insert threshold 
				while ((ret = send(socket_desc,buf,msg_len,0))<0){
					if(errno ==EINTR) continue;
					ret = close(socket_desc);
					//free buffer
					free(args->client_addr);
					free(args);
					//exit
					pthread_exit(NULL);
				}
				//clear buff
				memset(buf, 0 , buf_len);
                
				//recieve threshold value from client
			    while ((recv_bytes = recv(socket_desc,buf ,buf_len,0))<0){
					if(errno == EINTR) continue;
					ret = close(socket_desc);
					//free buffer
					free(args->client_addr);
					free(args);
					//exit
					pthread_exit(NULL);
				}
                if(DEBUG)
                    fprintf(stderr, "buf: %s\n",buf);
				/********************************************************************
				 * 	OSS: NON HAI FATTO UN CONTROLLO SULL'INPUT DEL VALORE DI SOGLIA *
				 * ******************************************************************/
				//conversion of string to integer for usage of true value.
				char* end;
				const long get_value = strtol(buf,&end,10);
				threshold = (int)get_value;
				
				//clear buff
				memset(buf, 0 , buf_len);
				/*if(DEBUG)
					fprintf(stderr, "buf : %s\n", buf);	*/
				//inizializzare messaggio di avvio sistema videosorveglianza
				sprintf(buf, STARTVIDEO_MESS);
				msg_len = strlen(buf);
				if(DEBUG)
					fprintf(stderr, "buf: %s\n", buf);
				//send starting message
				while ((ret = send(socket_desc,buf,msg_len,0))<0){
					if(errno ==EINTR) continue;
					ret = close(socket_desc);
					//free buffer
					free(args->client_addr);
					free(args);
					//exit
					pthread_exit(NULL);
				}
                //clean buff
                memset(buf, 0, buf_len);
					
				//clean database but not the foto 
				system("rm -rf ./files/im*.jpg");

				/**
				 * 
				 * Start mutex esclusion
				 * 
				 * */
				ret = sem_wait(&Rasputin);
				ERROR_HELPER(ret, "Wait on semaphore Rasputin");
				printf("\n Do first photo \n");
				takePhoto(count);
				
				ret = sem_post(&Rasputin);
				ERROR_HELPER(ret,"Exiting from semaphore Rasputin");
				/**
				 * 
				 * End mutex esclusion
				 * 
				 * */
				do{
					//do the second photo in mutex esclusion
					ret = sem_wait(&Rasputin);
					ERROR_HELPER(ret,"Wait on sempaphore Rasputin");
					
					count++;
					takePhoto(count);
		                        sleep(5);
					
					ret = sem_post(&Rasputin);
					ERROR_HELPER(ret,"Exiting from semaphore Rasputin");
					//try to implement a signal handler to stop the cicle 
					//(example when u come back to home and want to stop the service)
				}while(there_is_movement(threshold,count));
				
		
				
				//at this point we are sure that there's a detection
				//so as server we send at client the error (the image will be saved on server)
			
				//initialize allarm message
				memset(buf , 0 , buf_len);
				if(DEBUG) 
					fprintf(stderr, "buf: %s\n", buf);
				sprintf(buf , ALLARM_MESS);
				msg_len = strlen(buf);
				if(DEBUG) 
					fprintf(stderr, "buf: %s\n", buf);
				//send allarm message
				while ((ret = send(socket_desc, buf, msg_len, 0)) < 0){
					if (errno == EINTR) continue;
						ret = close(socket_desc);
					//free buffer
					free(args->client_addr); 
					free(args);
					//exit
					pthread_exit(NULL);
				}							
			}
			//NO (don't quit the program but send the user on the start menu)
			else if(recv_bytes == NO_LEN && !memcmp(buf, NO, NO_LEN)){
				memset(buf, 0 , buf_len);
				if(DEBUG)
					fprintf(stderr, "buf : %s\n", buf);
				control = FALSE;
				//clean the terminal's server
				system("clear");
				goto NS;
			}		
		}//end videosurveillance
    }//end of mainloop
    

    // close socket
    ret = close(socket_desc);
    ERROR_HELPER(ret, "Cannot close socket for incoming connection");
    if (DEBUG)
        fprintf(stderr, "Thread created to handle the request has completed.\n");
    system("clear");
    //free buffer
    free(args->client_addr); 
    free(args);
    //exit
    pthread_exit(NULL);
}

//main function
int main(int argc, char *argv[]){
    int ret;
    //semaphore initialized
    ret=sem_init(&Rasputin,0,1);
    ERROR_HELPER(ret,"Failed to inizialize semaphore");
    /**Thread Polling (we can start it only for doing some photo used for debugging)
    pthread_t poll_thread;
    ret=pthread_create(&poll_thread,NULL,polling,&Rasputin);
    PTHREAD_ERROR_HELPER(ret,"Could not create thread");
    ret=pthread_detach(poll_thread);
    PTHREAD_ERROR_HELPER(ret , "Could not detach thread");
    //End Polling
    */
    int socket_desc, client_desc;
    // some fields are required to be filled with 0
    struct sockaddr_in server_addr = {0};
    int sockaddr_len = sizeof(struct sockaddr_in); // we will reuse it for accept()
    // initialize socket for listening
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    ERROR_HELPER(socket_desc, "Could not create socket");

    server_addr.sin_addr.s_addr = INADDR_ANY; // we want to accept connections from any interface
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT); // don't forget about network byte order!

    /* We enable SO_REUSEADDR to quickly restart our server after a crash:
     * for more details, read about the TIME_WAIT state in the TCP protocol */
    int reuseaddr_opt = 1;
    ret = setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    ERROR_HELPER(ret, "Cannot set SO_REUSEADDR option");

    // bind address to socket
    ret = bind(socket_desc, (struct sockaddr *)&server_addr, sockaddr_len);
    ERROR_HELPER(ret, "Cannot bind address to socket");

    // start listening
    ret = listen(socket_desc, MAX_CONN_QUEUE);
    ERROR_HELPER(ret, "Cannot listen on socket");

    // we allocate client_addr dynamically and initialize it to zero
    struct sockaddr_in *client_addr = calloc(1, sizeof(struct sockaddr_in));

	puts("I'm waiting for someone :)) ");
    // loop to manage incoming connections spawning handler threads
    while (1){
        
        // accept incoming connection
        client_desc = accept(socket_desc, (struct sockaddr *)client_addr, (socklen_t *)&sockaddr_len);
        if (client_desc == -1 && errno == EINTR)
            continue; // check for interruption by signals
        ERROR_HELPER(client_desc, "Cannot open socket for incoming connection");

        if (DEBUG)
            fprintf(stderr, "Incoming connection accepted...\n");

        pthread_t thread;

        // put arguments for the new thread into a buffer
        handler_args_t *thread_args = malloc(sizeof(handler_args_t));
        thread_args->socket_desc = client_desc;
        thread_args->client_addr = client_addr;

        ret = pthread_create(&thread, NULL, connection_handler, (void *)thread_args);
        PTHREAD_ERROR_HELPER(ret, "Could not create a new thread");

        if (DEBUG)
            fprintf(stderr, "New thread created to handle the request!\n");

        ret = pthread_detach(thread); // I won't phtread_join() on this thread
        PTHREAD_ERROR_HELPER(ret, "Could not detach the thread");
        // we can't just reset fields: we need a new buffer for client_addr!
        client_addr = calloc(1, sizeof(struct sockaddr_in));
    }

    exit(EXIT_SUCCESS); // this will never be executed
}
