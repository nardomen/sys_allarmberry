#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

#include "takePhoto.h"
#include "poll.h"

void* polling(void* arg){
	sem_t* Rasputin=(sem_t*)arg;
	int count;
	while(1){
		sem_wait(Rasputin);
		takePhoto(count);
		count++;
		fprintf(stderr , "Thread ceck : YES");
		sem_post(Rasputin);
		sleep(10);
		
	}
}
