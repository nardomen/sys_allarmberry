#ifndef CLIENT_H
	#define CLIENT_H

	// macro to simplify error handling
	#define GENERIC_ERROR_HELPER(cond, errCode, msg) do {               \
        if (cond) {                                                 \
            fprintf(stderr, "%s: %s\n", msg, strerror(errCode));    \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

	#define ERROR_HELPER(ret, msg)          GENERIC_ERROR_HELPER((ret < 0), errno, msg)
	#define PTHREAD_ERROR_HELPER(ret, msg)  GENERIC_ERROR_HELPER((ret != 0), ret, msg)

	/* Configuration parameters */
	#define DEBUG           1   // display debug messages
	#define SERVER_ADDRESS  "192.168.1.6"
	#define SERVER_COMMAND  "quit"
	#define SERVER_PORT     2015

    #define PHOTO 			"searchphoto"
    #define PHOTO_LEN 		strlen(PHOTO)
    
    #define FALSE 			0
    #define TRUE 			1

    #define FOUND 			"found"
    #define FOUND_LEN 		strlen(FOUND)
    #define NOT_FOUND		"notfound"
    #define NOT_FOUND_LEN	strlen(NOT_FOUND)

    #define SETTINGS_MESS   "Insert valid option.\nWhat do you want modify?\nResolution\nBrightness\nExposure\nContrast\n"
    
    
    /** variables for the video surveillance */
    #define SURVEILLANCE "videosurveillance"
    #define SURVEILLANCE_LEN strlen(SURVEILLANCE)
    
    #define YES "yes"
    #define YES_LEN strlen(YES)
    #define NO "no"
    #define NO_LEN strlen(NO)
    #define STARTSERVICE_MESS "Insert valid option.\n Do u want to start video survellaince ? [ Y:Yes | N:No ]"
    
    

#endif
